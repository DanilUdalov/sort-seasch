class ProductsController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :current_product, only: [:show, :edit, :destroy, :update]

  def index
    @products = Product.all.page(params[:page]).order(sort_column + ' ' + sort_direction)
    @products = @products.search(params[:search]) if params[:search].present?
  end

  def show
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product
    else
      render :new
    end
  end

  def update
    if @product.update(product_params)
      redirect_to @product
    else
      render 'edit'
    end
  end

  def destroy
    @product.destroy
    redirect_to root_path
  end

  private

  def product_params
    params.require(:product).permit(:name, :price)
  end

  def sort_column
    Product.column_names.include?(params[:sort]) ? params[:sort] : 'name'
  end

  def sort_direction
    %w(asc desc).include?(params[:direction]) ? params[:direction] : 'asc'
  end

  def current_product
    @product = Product.find(params[:id])
  end
end
