class Product < ActiveRecord::Base
  def self.search(search)
    search ? where('name like ?', "%#{search}%") : all
  end

  self.per_page = 10
end
